pragma solidity ^0.4.18;


contract iotContract  {
  
  struct Device {
    bytes32 deviceUUID;
    uint id;
    bool requireMaintenance;
    bool inMaintenanceMode;
    bool authenticated;
  }

 
  address public owner;
  uint deviceCounter;

  mapping(uint => Device) public devices;
  mapping(bytes32 => uint) public deviceIDs;
  mapping (bytes32=> uint) ipfsRecordCounter;
  mapping (bytes32 => mapping (uint => string) ) public ipfsHash;
  

 

  event needsMaintenanceEvent (
    bytes32 deviceUUID,
    bool _needsMaintenance
  );

  event inMaintenanceModeEvent (
    bytes32 deviceUUID,
    bool _inMaintenanceMode
  );

  event registerDeviceEvent (
    uint indexed _id,
    bytes32 deviceUUID,
    bool _requireMaintenance,
    bool _maintenance
  );
  
  event logData (
      uint deviceId,
      bytes32 deviceUUID,
      string IPFS,
      uint temp
  );
  
  modifier onlyOwner() {
      require(msg.sender==owner);
      _;
  }
  
  modifier onlyRegisteredDevice(bytes32 _deviceUUID) {
      var _id = deviceIDs[_deviceUUID];
      require(devices[_id].authenticated);
      _;
  }



  // constructor
  function iotContract() public {
       owner = msg.sender;
  }

  // register device
  function registerDevice(bytes32 _deviceUUID) onlyOwner() public {
    // a new device
    deviceCounter++;

    // store this device
    devices[deviceCounter] = Device(
        _deviceUUID,
      deviceCounter,
      false,
      false,
      true
    );
    
    deviceIDs[_deviceUUID] = deviceCounter;

    // trigger the event
    registerDeviceEvent(deviceCounter, _deviceUUID, false, false);
  }
  
  
  function logTemp (bytes32 _deviceUUID, string _ipfs, uint _temp) onlyRegisteredDevice(_deviceUUID) {
      
      ipfsRecordCounter[_deviceUUID]++;
      ipfsHash[_deviceUUID][ipfsRecordCounter[_deviceUUID]] = _ipfs;
      
      
      logData(deviceIDs[_deviceUUID],_deviceUUID,_ipfs,_temp);
      
  }
  
  function getNumberOfIpfsRecordsForDevice(bytes32 _deviceUUID) onlyRegisteredDevice(_deviceUUID)  constant returns (uint){
      return ipfsRecordCounter[_deviceUUID];
  }

  // fetch the total number of devices 
  function getNumberOfDevices() public constant returns (uint) {
    return deviceCounter;
  }
  
  function setNeedsMaintenance(bytes32 _deviceUUID, bool _requireMaintenance) onlyRegisteredDevice(_deviceUUID){
      var _id = deviceIDs[_deviceUUID];
      devices[_id].requireMaintenance = _requireMaintenance;
      needsMaintenanceEvent ( devices[_id].deviceUUID, _requireMaintenance);
  }
  
  function getDeviceId(bytes32 _deviceUUID) public constant returns(uint) {
      return deviceIDs[_deviceUUID];
      
  }
  
   // fetch and returns all device IDs that needed maintanance
  function deviceForMaintenance() public constant returns (uint[]) {
    // we check whether there is at least one robot
    if(deviceCounter == 0) {
      return new uint[](0);
    }

    // prepare intermediary array
    uint[] memory deviceIds = new uint[](deviceCounter);


    uint numberOfDeviceForMaintenance = 0;
    // iterate over devices
    for (uint i = 1; i <= deviceCounter; i++) {
      // keep only the ID of devices not sold yet
      if (devices[i].requireMaintenance) {
        deviceIds[numberOfDeviceForMaintenance] = devices[i].id;
        numberOfDeviceForMaintenance++;
      }
    }

    // copy the deviceIds array into the smaller forMaintenance array
    uint[] memory forMaintenance = new uint[](numberOfDeviceForMaintenance);
    for (uint j = 0; j < numberOfDeviceForMaintenance; j++) {
      forMaintenance[j] = deviceIds[j];
    }
    
    return (forMaintenance);
  }


  function setInMaintenanceMode(bytes32 _deviceUUID, bool _inMaintenanceMode) public onlyRegisteredDevice(_deviceUUID) {
    var _id = deviceIDs[_deviceUUID];
    Device storage device = devices[_id];
    device.inMaintenanceMode = _inMaintenanceMode;

    inMaintenanceModeEvent(devices[_id].deviceUUID, device.inMaintenanceMode);
  }

}
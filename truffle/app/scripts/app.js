


import { default as Web3} from 'web3';
import { default as uuid} from 'uuid-by-string';
import { default as contract } from 'truffle-contract'


import iot_contract_artifacts from '../../build/contracts/iotContract.json'

var iotDeviceContract = contract(iot_contract_artifacts);

let account;

let data;



// var adminAddr = "0x3859A2b4EdCe17F724B6d592b11D783038B6A9BF";
// console.log("this is address",adminAddr);


// var keccakvalue = web3.sha3(web3.toHex(adminAddr),{ encoding: 'hex' });
// console.log("this is keccakvalue",keccakvalue);


let hisData;




window.setStaking = function() {
  let tokenNumber = convertTokenToEightDecimal($("#inputStake").val());

  try {
    daleCoin.deployed().then(function(contractInstance) {
      contractInstance.setStakingRequirement(tokenNumber, {from: account}).then(function(result) {
      
        $("#stakeStatus").html("Changed Successfully");
      });
    });
  } catch (err) {
    console.log(err);
  }
}



function loadTable(tableId, fields, data) {
  //$('#' + tableId).empty(); //not really necessary
  var rows = '';
  $.each(data, function(index, item) {
      var row = '<tr>';
      $.each(fields, function(index, field) {
          row += '<td>' + item[field+''] + '</td>';
      });
      rows += row + '<tr>';
  });
  $('#' + tableId + ' tbody').html(rows);
}

$('#btn-update').click(function(e) {

  var data2 = [
    { field1: 'new value a1', field2: 'new value a2', field3: 'new value a3', field4 : 'newVal' },
    { field1: 'new value b1', field2: 'new value b2', field3: 'new value b3' },
    { field1: 'new value c1', field2: 'new value c2', field3: 'new value c3' }
    ];
  // var data3 = myData;
  console.log("in function",hisData);

  loadTable('data-table', ['field1', 'field2', 'field3','field4'], hisData);
});


window.getDataOfDevice = function() {
  console.log("calling data for device");
  let deviceID = $("#inputId").val();
  console.log(deviceID);
  
  try {
    iotDeviceContract.deployed().then(function(contractInstance) {
      contractInstance.logData({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {
        if (error)
          console.log('Error in myEvent event handler: ' + error);
        else
          
          console.log(eventResult.length);
          let last = eventResult.length -1;
          let dataArr = [];
          let deviceData;
          for(let i = 0; i<= last; i++) {

            if(eventResult[i].args.deviceId.toNumber()== deviceID) {

              var dataObj = {label: deviceID,  y: (eventResult[i].args.temp.toNumber())/100};
              dataArr.push(dataObj)

            }

          }

          if(dataArr.length > 6) {

            deviceData = dataArr.slice(Math.max(dataArr.length - 6, 1))

          }else {

            deviceData = dataArr;

          }

          var devChart = new CanvasJS.Chart("deviceChart", {
            theme: "light1", // "light2", "dark1", "dark2"
            animationEnabled: false, // change to true		
            title:{
                text: "Temperature Graph of Checked Device"
            },
            data: [
            {
                // Change type to "bar", "area", "spline", "pie",etc.
                type: "column",
                dataPoints: deviceData 
            }
            ]
            });
            devChart.render();

      });
    });
  } catch (err) {
    console.log(err);
  }
}






$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }


    web3.eth.getAccounts(function (err, accs) {
    
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }

      if (accs.length === 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.")
        return
      }

      account = accs[0]
 

    })

    data =  [
      { label: "null",  y: 0  },
      { label: "null", y: 0  },
      { label: "null", y: 0  },
      { label: "null",  y: 0  },
      { label: "null",  y: 0  }
    ]

    hisData = [];

  
  

    iotDeviceContract.setProvider(web3.currentProvider);



    function reload() {

      var chart = new CanvasJS.Chart("chartContainer", {
        theme: "light1", // "light2", "dark1", "dark2"
        animationEnabled: false, // change to true		
        title:{
            text: "Live Temperature Graph - All Devices"
        },
        data: [
        {
            // Change type to "bar", "area", "spline", "pie",etc.
            type: "column",
            dataPoints: data 
        }
        ]
        });
        chart.render();

      iotDeviceContract.deployed().then(function(contractInstance) {

        contractInstance.getNumberOfDevices({from: account}).then(function(result) {

          console.log(result.toNumber());
 
          $("#totalDevices").html(result.toNumber());
        });

        contractInstance.logData({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {
          if (error)
            console.log('Error in myEvent event handler: ' + error);
          else
          console.log(eventResult[0].args);
            // console.log('myData: ' + JSON.stringify(eventResult[0].args));
            console.log(eventResult.length);
            console.log(eventResult[30].args);
            var start = eventResult.length - 6;
            var last = eventResult.length -1;
            let newData = [];
            for(let i = start; i<= last; i++) {

              var dataObj = {label: eventResult[i].args.deviceId.toNumber(),  y: (eventResult[i].args.temp.toNumber())/100}
              newData.push(dataObj);

            }

            data = newData;

            let tableData = [];

            for(let n=0; n<eventResult.length; n++) {

              tableData.push({ field1: eventResult[n].args.deviceId.toNumber(), field2: eventResult[n].args.deviceUUID, field3: eventResult[n].args.IPFS, field4 : (eventResult[n].args.temp.toNumber())/100 })
              
            }

            hisData = tableData;
    

        });

       
    });

    

    }

    setInterval(reload, 3000);
});






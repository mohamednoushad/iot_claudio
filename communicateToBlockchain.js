//file that makes transactions on blockchain
const ethers = require('ethers');
const web3 = require('web3');
const configuration = require('./config');
const helper = require('./helper');

let provider = ethers.getDefaultProvider('rinkeby');

abi = [{"constant":false,"inputs":[{"name":"_deviceUUID","type":"bytes32"},{"name":"_ipfs","type":"string"},{"name":"_temp","type":"uint256"}],"name":"logTemp","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"deviceUUID","type":"bytes32"},{"indexed":false,"name":"_needsMaintenance","type":"bool"}],"name":"needsMaintenanceEvent","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"deviceUUID","type":"bytes32"},{"indexed":false,"name":"_inMaintenanceMode","type":"bool"}],"name":"inMaintenanceModeEvent","type":"event"},{"constant":false,"inputs":[{"name":"_deviceUUID","type":"bytes32"}],"name":"registerDevice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_id","type":"uint256"},{"indexed":false,"name":"deviceUUID","type":"bytes32"},{"indexed":false,"name":"_requireMaintenance","type":"bool"},{"indexed":false,"name":"_maintenance","type":"bool"}],"name":"registerDeviceEvent","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"deviceId","type":"uint256"},{"indexed":false,"name":"deviceUUID","type":"bytes32"},{"indexed":false,"name":"IPFS","type":"string"},{"indexed":false,"name":"temp","type":"uint256"}],"name":"logData","type":"event"},{"constant":false,"inputs":[{"name":"_deviceUUID","type":"bytes32"},{"name":"_inMaintenanceMode","type":"bool"}],"name":"setInMaintenanceMode","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_deviceUUID","type":"bytes32"},{"name":"_requireMaintenance","type":"bool"}],"name":"setNeedsMaintenance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"_generation","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"constant":true,"inputs":[],"name":"deviceForMaintenance","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"deviceIDs","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"devices","outputs":[{"name":"deviceUUID","type":"bytes32"},{"name":"id","type":"uint256"},{"name":"requireMaintenance","type":"bool"},{"name":"inMaintenanceMode","type":"bool"},{"name":"authenticated","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_deviceUUID","type":"bytes32"}],"name":"getDeviceId","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getNumberOfDevices","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_deviceUUID","type":"bytes32"}],"name":"getNumberOfIpfsRecordsForDevice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"bytes32"},{"name":"","type":"uint256"}],"name":"ipfsHash","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"}];
    
contractAddress = "0x62DB8A3b9c77E4342C6f01cd9870c34C3F8Cbee0";



let privateKey;
let wallet;
let contract;


async function getDevicePrivateKey() {
    
    let filePath = await configuration.datadir();
    var keyObject = await helper.getKeyObject(filePath);

    if(keyObject){

        let password = await configuration.deviceId();
        bufferPrivateKey  = await helper.getPrivateKey(password,keyObject);
        privateKey = "0x"+bufferPrivateKey.toString('hex');
        wallet = new ethers.Wallet(privateKey,provider);
        contract = new ethers.Contract(contractAddress, abi, wallet);
        return privateKey;

    }else{

        console.log("Device has no wallet");
    }
   
}



 module.exports = {

     logData : async function(deviceId,ipfsHash,temp){

         console.log("calling logging data",deviceId,ipfsHash,temp);

         var tempWithoutDecimals = Math.floor(temp*100);
        
         try {

         const transaction = await contract.logTemp(web3.utils.fromAscii(deviceId),ipfsHash,tempWithoutDecimals);
         const result = await provider.waitForTransaction(transaction.hash);
         console.log(result);
         return result;

         } catch(e){
             console.log(e);
         }
         
     },

     registerDevice : async function(hash,ownerPrivateKey) {
          console.log("calling registering device function");
          let registerWallet = new ethers.Wallet(ownerPrivateKey,provider);
          let registerContract = new ethers.Contract(contractAddress, abi, registerWallet);

         try{

         const transaction = await registerContract.registerDevice(web3.utils.fromAscii(hash));
         const result = await provider.waitForTransaction(transaction.hash);
         console.log(result);
         return result;

         }catch(e){
             console.log(e);
         }
     },

     loadDeviceWalletWithEther : async function(ownerPrivateKey,accountAddress) {

        var wallet = new ethers.Wallet(ownerPrivateKey,provider);
      
        let tx = {to: accountAddress,
                  value: ethers.utils.parseEther('0.5') };
        
        try {

            var loadEther = await wallet.sendTransaction(tx);
            var result = await provider.waitForTransaction(loadEther.hash);
            console.log(result);
            return result;

        } catch (e){
            console.log(e);
        }
        

     }
 }

    try {

         getDevicePrivateKey()
    
        } catch(e){
        
        console.log(e);

     }

    


 




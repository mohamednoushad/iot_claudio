//file to receive mqtt messages if needed to be implemented for between device communication
var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://test.mosquitto.org')
 



  client.on('connect', function () {
    client.subscribe('presence', function (err) {
      if (!err) {
        client.on('message', function (topic, message) {
          // message is Buffer
          //console.log(message.toString());
          temp = message.toString();
          //console.log(parseInt(temp));
          temperatureCheck(parseInt(temp));
        })
      }
    })
  })

  function temperatureCheck(inputTemp){
    console.log(inputTemp);
    if(inputTemp>28){
      console.log("Temperature is High, needs to record on blockchain");
    }
  }
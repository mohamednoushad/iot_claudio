const fs = require('fs');
var keythereum = require("keythereum");

let checkFileExists = s => new Promise(r=>fs.access(s, fs.F_OK, e => r(!e)))


module.exports = {

checkFileExist : async function(filepath) {

        var condition = await checkFileExists(filepath);

        if(condition){

            files = await fs.readdirSync(filepath);

            if (files.length){

                return true;

            }else{

                return false;
            }

        }else{

            return false;
        }
    },

    getKeyObject : async function(filepath) {
   
        files = await fs.readdirSync(filepath)

        if(files.length){

            file =  await fs.readFileSync(filepath+'/'+files[0],'utf8');
            return file;

        }else {
            
            return false;
        }
        
    },

    getPrivateKey : async function(password,keyObject) {

        return await keythereum.recover(password,JSON.parse(keyObject));
    }

}



//file to register the device on the platform which can only be done by the admin / owner
var keythereum = require("keythereum");
const mkdirp = require('async-mkdirp');
const getUuid = require('uuid-by-string');
const configuration = require('./config');
const connection = require('./communicateToBlockchain');
const helper = require('./helper');


let ownerPrivKey = '0xe04a43177c743a56a34137c4f76b9e7bbbeee6c5022253cda9bda128527842e8';


let datadir= configuration.datadir();   


//keystore required params
let params = { keyBytes: 32, ivBytes: 16 };
let dk = keythereum.create(params);
let password = configuration.deviceId();
let options = {kdf: "pbkdf2",cipher: "aes-128-ctr",kdfparams: {c: 262144,dklen: 32,prf: "hmac-sha256"}};

//create wallet and load ethers for the account

async function createDeviceWallet() {

    const checkIfhasWallet = await helper.checkFileExist(datadir)

    if(!checkIfhasWallet){

        let keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
        const directory = await mkdirp(datadir);
        const fileSaved = await keythereum.exportToFile(keyObject,datadir);
        var account = "0x"+keyObject.address;
        const loadingBalance = await connection.loadDeviceWalletWithEther(ownerPrivKey,account);
        return true;

     } else {

        console.log("Device Already Registerd, wallet already exist");

        return false;

     }
}

async function registerDeviceOnBlockchain() {

    const deviceId =  await configuration.deviceId();
    const deviceUUID = await getUuid(deviceId).replace(/-/g, '');
    const transaction = await connection.registerDevice(deviceUUID,ownerPrivKey);
    console.log(transaction);
    return transaction;

}





async function main() {

    try {

        const walletStatus = await createDeviceWallet();

        if (walletStatus) {

            const registerStatus = await registerDeviceOnBlockchain();

        } else {

            console.log("This Device is Already Registered");
        }

  
    } catch(e) {

        console.log(e)

    }
    
}

main();

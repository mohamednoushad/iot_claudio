//the encryption helper file to encrypt and decrypt the data
var CryptoJS = require("crypto-js");
const configuration = require('./config');
const {machineIdSync} = require('node-machine-id');

let deviceId = configuration.deviceId();
 

module.exports = {
    encryptMessage : async function(data){
        console.log("calling encryption function",data);
        // const encryptedData = await  CryptoJS.AES.encrypt(JSON.stringify(data), machineIdSync({original: true})).toString();
        const encryptedData = await  CryptoJS.AES.encrypt(JSON.stringify(data), deviceId).toString();
        console.log(encryptedData);
        return encryptedData;
    },

    decryptMessage : function(data) {
        return JSON.parse(CryptoJS.AES.decrypt(data, machineIdSync({original: true})).toString(CryptoJS.enc.Utf8));
    }

}


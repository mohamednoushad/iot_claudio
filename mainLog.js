//main file to make actions when the incoming temperature is more than the set alarming temperature
const connection = require('./communicateToBlockchain');
const configuration = require('./config');
const getUuid = require('uuid-by-string')
const encryption = require('./encryption');
const ipfs = require('./ipfsApi');


let deviceId = configuration.deviceId();


module.exports = {

  insert : async function (temperature,timestamp){

    let data = [{deviceID: deviceId},
                {temperatureValue:temperature},
                {temperatureLimit : configuration.unsafeTemperature},
                {time: timestamp}]
  
    if(temperature>configuration.unsafeTemperature) {

      encryptedData = await encryption.encryptMessage(data);
      console.log(encryptedData);
      ipfsHash = await ipfs.addToIpfs(encryptedData);
      console.log(ipfsHash);
      encryptedIpfsHash = await encryption.encryptMessage(ipfsHash[0].hash);
      deviceUUID = await getUuid(deviceId).replace(/-/g, '');
      const transaction =  await connection.logData(deviceUUID,encryptedIpfsHash,temperature);
      console.log("Blockchain Transaction Made");
      if(transaction.status){
        console.log("Transaction Successful");
        return true;
      }else{
        console.log("Transaction Failed");
        return false;
      }
     

    }else{

      console.log("Device in Safe Temperature, No need to log Data");
      return true;

    }
  
  }

}






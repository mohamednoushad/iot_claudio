const RaspiSensors = require('raspi-sensors');
const sleep = require ('await-sleep');
const logData = require('./mainLog')

var BMP180 = new RaspiSensors.Sensor({
  type    : "BMP180",
  address : 0x77
}, "BMP180");


// BMP180.fetchInterval(function(err, data) {
//     if(err) {
//         console.error("An error occured!");
//         console.error(err.cause);
//         return;
//     }

//     if(data.type === "Temperature") {
//        console.log(data);
//        console.log(data.value);
//        var temperature = data.value;
//        var timestamp = data.timestamp;
//        logData.insert(temperature,timestamp);

//     }

   
// }, 10); // Fetch data every 10 seconds

async function getData() {

    BMP180.fetch(async function(err,data){
         if(data.type==="Temperature") {
             console.log(data);
            var temperature = data.value;
            var timestamp = data.timestamp;
            const dataStatus = await logData.insert(temperature,timestamp);

            if(dataStatus){

                await sleep(60000);
                return getData();

            } else {

                console.log("Something went worng");
                console.log("Giving a delay of 2 minutes");
                await sleep(120000);
                return getData();


            }
             
        }
        
    });

}

getData();







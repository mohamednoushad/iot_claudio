var os = require('os');
const {machineId, machineIdSync} = require('node-machine-id')

let prefix = os.homedir();


module.exports = {

    datadir : function() {
        return prefix+'/wallet/keystore';
    },

    deviceId : function() {
        return machineIdSync({original: true});
    },

    unsafeTemperature : 28

 
}
    




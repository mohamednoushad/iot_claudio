// file which posts data to IPFS and also gets data from IPFS
const ipfsAPI = require ('ipfs-api');

const ipfs_net = ipfsAPI('ipfs.infura.io', '5001', {protocol: 'https'});

module.exports = {

    addToIpfs : async function(data) {

        console.log("calling add to ipfs function");
        let initialBufferData = Buffer.from(data);
        let bufferData = new Buffer(initialBufferData);

        try {

            const ipfsHashStatus = await ipfs_net.files.add(bufferData);

            return ipfsHashStatus;

        }catch(e){
            
            console.log(e);
        }

        

    },

    getFromIpfs : async function(ipfsHash) {

        return await ipfs_net.cat(ipfsHash).toString('utf8');
   
    }
}











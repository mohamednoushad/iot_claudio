pragma solidity ^0.4.18;


contract Clone  {
  // Custom types
  
  struct Device {
    bytes32 hash;
    uint id;
    bool requireMaintenance;
    bool inMaintenanceMode;
  }

  // state variables
  address public owner;
  uint deviceCounter;
 
  mapping(uint => Device) public devices;
  mapping(uint => bytes32) public hashes;

 

  event needsMaintenanceEvent (
    bytes32 hash,
    bool _needsMaintenance
  );

  event inMaintenanceModeEvent (
    bytes32 hash,
    bool _inMaintenanceMode
  );

  event registerDeviceEvent (
    uint indexed _id,
    bytes32 _hash,
    bool _requireMaintenance,
    bool _maintenance
  );
  
  modifier onlyOwner() {
      require(msg.sender==owner);
      _;
  }



  // constructor
  function Clone(uint _generation) public {
       owner = msg.sender;
  }

  // register device
  function registerDevice(bytes32 _hash) public {
    // a new device
    deviceCounter++;

    // store this device
    devices[deviceCounter] = Device(
        _hash,
      deviceCounter,
      false,
      false
    );

    // trigger the event
    registerDeviceEvent(deviceCounter, _hash, false, false);
  }

  // fetch the total number of devices 
  function getNumberOfDevices() public constant returns (uint) {
    return deviceCounter;
  }
  
  function setNeedsMaintenance(uint _id, bool _requireMaintenance) {
      devices[_id].requireMaintenance = _requireMaintenance;
      needsMaintenanceEvent ( devices[_id].hash, _requireMaintenance);
  }
  
  function getDeviceId(bytes32 _hash) public constant returns(uint[]) {
      if(deviceCounter == 0) {
      return new uint[](0);
    }
      uint[] storage deviceId;
      for (uint i = 1; i <= deviceCounter; i++) {
      // keep only the ID of devices not sold yet
      if (devices[i].hash==_hash) {
        deviceId.push(devices[i].id);
      }
    }
    return deviceId;
      
  }
  
   // fetch and returns all device IDs that needed maintanance
  function deviceForMaintenance() public constant returns (uint[]) {
    // we check whether there is at least one robot
    if(deviceCounter == 0) {
      return new uint[](0);
    }

    // prepare intermediary array
    uint[] memory deviceIds = new uint[](deviceCounter);


    uint numberOfDeviceForMaintenance = 0;
    // iterate over devices
    for (uint i = 1; i <= deviceCounter; i++) {
      // keep only the ID of devices not sold yet
      if (devices[i].requireMaintenance) {
        deviceIds[numberOfDeviceForMaintenance] = devices[i].id;
        numberOfDeviceForMaintenance++;
      }
    }

    // copy the deviceIds array into the smaller forMaintenance array
    uint[] memory forMaintenance = new uint[](numberOfDeviceForMaintenance);
    for (uint j = 0; j < numberOfDeviceForMaintenance; j++) {
      forMaintenance[j] = deviceIds[j];
    }
    
    return (forMaintenance);
  }


 

  function setInMaintenanceMode(uint _id, bool _inMaintenanceMode) public {
    Device storage device = devices[_id];
    device.inMaintenanceMode = _inMaintenanceMode;

    inMaintenanceModeEvent(devices[_id].hash, device.inMaintenanceMode);
  }



}
